namespace Jeu_Du_Gobelet
{
    /*
    La Classe Joueur est défini par :
    + Un Nom de joueur
    + Une mise, c'est le montant du pari
    + Un gobelet, c'est un numéro d'indice dans le tableau de gobelet de la classe Jeu
    */
    public class Joueur
    {
        // variable() d'instance
        public string nom;
        public int mise;
        public int gobelet;

        // Constructeur(s)
        public joueur(string _nom, int _mise, int _gobelet)
        {
            this.nom = _nom;
            this.mise = _mise;
            this.gobelet = _mise;
        }
    }

    
    //La classe Gobelet représente un gobelet, il peut être vide ou bien contenir le jeton
    
    class Gobelet
    {
        // variable d'instance
        private bool jeton;

        // Constructeur(s)
        private jeton() 
        {
            this.jeton = FAUX;
        }
        private jeton(FAUX)
        {
            this.jeton = FAUX;
        }

        // méthode d'instance
        public check(Jeu)
        {
            if(jeton() = true)
            {
                return VRAI;
            }
            else
            {
                return FAUX;
            }
        }

        // méthode de classe
        public void vide()
        {
            return Gobelet(FAUX);
        }

        public void gagnant()
        {
            return Gobelet(VRAI);
        } 
        // variables d'instance privées
        private int[] joueurs = new int[];
        private int[] gobelet = new int[];
        private const mise_maximum;

        // Constructeur
        public Jeu(int nombre_joueurs, int nombre_gobelets)
        {
            // Gobelets
            this.gobelets = new Gobelet [nombre_gobelets];

            // Choix aléatoire de l'indice gagnant
            int win = new Random().Next(0, nombre_gobelets);

            // Initialisation du tableau de gobelets
            for (int i=0, i < nombre_gobelets; i++);
            {
                this.gobelets[i] = i == win ? Gobelet.gagnant() : Gobelet.vide();
            }

            // Joueurs
            this.joueurs = new Joueur [nombre_joueurs];

            // Choix aléatoire de l'indice du joueur humain
            int humain = new Random().Next(0, nombre_joueurs);

            // Initialisation du tableau de joueurs robots
            for (int i=0; i < nombre_joueurs; i++)
            {
                // Cas joueur robot
                if (i != humain)
                {
                    this.joueurs[i] = 
                    new Joueur("Robot " + i.ToString(),
                    new Random().Next(1, mise_max),
                    new Random().Next(0, nombre_gobelets));
                }
                else 
                // Cas joueur humain
                {
                    this.joueurs[i] = new Joueur("Temp", 0, -1);
                }
            }

            Console.WriteLine("Le montant du gros lot est de " + GrosLot().ToString());
            Console.WriteLine("Paris par gobelets : " + StringParisParGobelets());

            // Création joueur humain
            int humain_mise = -1;
            int humain_gobelet = -1;

            Console.WriteLine("Quel est votre nom ?");
            string humain_nom = Console.ReadLine();

            while(humain_mise < 0 || humain_mise > mise_max)
            {
                Console.WriteLine("Saisir votre mise:");
                humain_mise = Convert.ToInt32(Console.ReadLine());
            }

            while (humain_gobelet < 0 || humain_gobelet > nombre_gobelets-1)
            {
                Console.WriteLine("Choisir un gobelet : ");
                humain_gobelet = Convert.ToInt32(Console.ReadLine());
            }

            this.joueurs[humain] = new Joueur(humain_nom, humain_mise, humain_gobelet);
        }
        // Méthodes d'instances
        private int GrosLot()
        {
            Somme_Mise = 0;
            for(int i = 0; i < nombre_joueurs; i++)
            {
                Somme_Mise += humain_mise;
            }
            return Somme_Mise;
        }
        private string StringParisParGobelets()
        {
            ParisParGobelets().ToString()
            {
            return "ici on a " + nombre_gobelets + "et :" + /*nombre de mise sur le gobelet 'i' */ ;
            }
        }
        private int[] ParisParGobelets()
        {
        int [] ParisParGobelets = [];
        for(int i = 0; i < nombre_gobelets; i++)
        {
            if(/*Le joueur parie sur un gobelet*/)
            {
                ParisParGobelets += [i];
            }
        }
        return ParisParGobelets;            
        }
        private int Gobelet_Gagnant()
        {
            for(int i = 0; i < nombre_gobelets; i++)
            {
                if (check() = false)
                {
                    return "Le gobelet gagnant est le gobelet " + i;
                }
            }
        }
        private bool[] Gagnants()
        {
            int [] tableau_joueur = [0];
            for(int i = 0; i < nombre_joueurs; i++)
            {
                tableau_joueur += [i];
            }
            if (Gobelet_Gagnant()=humain_mise())
            {
                tableau_joueur [i] = VRAI;
            }
            else
            {
                tableau_joueur [i] = FAUX;
            }
            return tableau_joueur;
        }
        private int NombreDeGagnants()
        {
            
        }
        private int TotalParisGagnants()
        {

        }
        public void Resultats()
        {
            bool [] gagnants = Gagnants();
            int [] parispargobelets = ParisParGobelets();
            int gobelet_gagnant = Gobelet_Gagnant();
            int gain = GrosLot() / TotalParisGagnants();

            Console.WriteLine("Les gagnants ont joué le gobelet : " + gobelet_gagnant.ToString());

            for (int i = 0; i < this.joueurs.Length; i++)
            {
                if(gagnants[i])
                {
                    Console.WriteLine("Joueur" + this.joueurs[i].nom + " mise " + this.joueurs[i].mise.ToString() + " et gagne " + (gain * this.joueurs[i].mise).ToString());
                }
           }
       }
    }
}
